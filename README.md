# README #

To run this project you'll need Xcode version 7.0+ with Support for Swift 2.0+

### BookTitles app Repository ###

* BookTitles is designed to ease fatigue from turning your head to read book titles while browsing book shelves.

* This version of the project builds BookTitles Version 1.0
* ##[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)##

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration -- none
* How to run tests -- currently no automated test cases are built into this project.
* Deployment instructions - iOS 7.0+

### Contribution guidelines ###

* Writing tests -- none
* Code review -- Code submitted to this repository requires approval from the Repository owner. - Joshua Armer

* Other guidelines -- none

### Who do I talk to? ###

* Joshua Armer - contact.joshuaarmer.com
* Other community or team contact -- N/A